# Translation of docs_krita_org_user_manual___gamut_masks.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-20 17:41+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../user_manual/gamut_masks.rst:1
msgid "Basics of using gamut masks in Krita."
msgstr "Conceptes bàsics sobre l'ús de màscares de gamma en el Krita."

#: ../../user_manual/gamut_masks.rst:10 ../../user_manual/gamut_masks.rst:15
msgid "Gamut Masks"
msgstr "Màscares de gamma"

#: ../../user_manual/gamut_masks.rst:19
msgid ""
"Gamut masking is an approach to color formalized by James Gurney, based on "
"the idea that any color scheme can be expressed as shapes cut out from the "
"color wheel."
msgstr ""
"L'emmascarament de la gamma és una aproximació al color formalitzada per en "
"James Gurney, basat en la idea que qualsevol combinació de colors es pot "
"expressar com a formes retallades de la roda de color."

#: ../../user_manual/gamut_masks.rst:21
msgid ""
"It originates in the world of traditional painting, as a form of planning "
"and premixing palettes. However, it translates well into digital art, "
"enabling you to explore and analyze color, plan color schemes and guide your "
"color choices."
msgstr ""
"S'origina en el món de la pintura tradicional, com a una forma de planificar "
"i premesclar les paletes. No obstant això, es tradueix bé en l'art digital, "
"el qual permet explorar i analitzar el color, planificar els esquemes de "
"color i guiar les vostres eleccions del color."

#: ../../user_manual/gamut_masks.rst:23
msgid "How does it work?"
msgstr "Com funciona?"

#: ../../user_manual/gamut_masks.rst:25
msgid ""
"You draw one or multiple shapes on top of the color wheel. You limit your "
"color choices to colors inside the shapes. By leaving colors out, you "
"establish a relationship between the colors, thus creating harmony."
msgstr ""
"Dibuixeu una o múltiples formes a la part superior de la roda de color. "
"Limiteu les vostre eleccions del color als colors dintre de les formes. En "
"deixar de banda els colors, s'estableix una relació entre els colors, creant "
"així una harmonia."

#: ../../user_manual/gamut_masks.rst:28
msgid ""
"Gamut masking is available in both the Advanced and Artistic Color Selectors."
msgstr ""
"L'emmascarament de la gamma està disponible en ambdós: el Selector avançat "
"del color i el Selector de color artístic."

# skip-rule: t-acc_obe
#: ../../user_manual/gamut_masks.rst:33
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney <https://gurneyjourney.blogspot."
"com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"`Enmascarament de la roda de color, part 1 per en James Gurney <https://"
"gurneyjourney.blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"

# skip-rule: t-acc_obe
#: ../../user_manual/gamut_masks.rst:34
msgid ""
"`The Shapes of Color Schemes by James Gurney <https://gurneyjourney.blogspot."
"com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`Les formes dels esquemes de color per en James Gurney <https://"
"gurneyjourney.blogspot.com/2008/02/shapes-of-color-schemes.html>`_"

# skip-rule: t-acc_obe
#: ../../user_manual/gamut_masks.rst:35
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube) <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Demostració de l'emmascarament de la gamma per en James Gourney (YouTube) "
"<https://youtu.be/qfE4E5goEIc>`_"

#: ../../user_manual/gamut_masks.rst:39
msgid "Selecting a gamut mask"
msgstr "Seleccionant una màscara de gamma"

#: ../../user_manual/gamut_masks.rst:41
msgid ""
"For selecting and managing gamut masks open the :ref:`gamut_mask_docker`:  :"
"menuselection:`Settings --> Dockers --> Gamut Masks`."
msgstr ""
"Per a seleccionar i gestionar les màscares de gamma, obriu l':ref:"
"`gamut_mask_docker`:  :menuselection:`Arranjament --> Acobladors --> "
"Màscares de gamma`."

#: ../../user_manual/gamut_masks.rst:45
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../user_manual/gamut_masks.rst:45
msgid "Gamut Masks docker"
msgstr "Acoblador Màscares de gamma"

#: ../../user_manual/gamut_masks.rst:47
msgid ""
"In this docker you can choose from several classic gamut masks, like the "
"‘Atmospheric Triad’, ‘Complementary’, or ‘Dominant Hue With Accent’. You can "
"also duplicate those masks and make changes to them (3,4), or create new "
"masks from scratch (2)."
msgstr ""
"En aquest acoblador podreu triar entre diverses màscares de gamma "
"clàssiques, com la «Tríada atmosfèrica», «Complementària» o «To dominant amb "
"accent». També podreu duplicar aquestes màscares i realitzar-hi canvis "
"(3,4), o crear màscares noves des de zero (2)."

#: ../../user_manual/gamut_masks.rst:49
msgid ""
"Clicking the thumbnail icon (1) of the mask applies it to the color "
"selectors."
msgstr ""
"En fer clic a la icona de la miniatura (1) de la màscara s'aplicarà als "
"selectors del color."

#: ../../user_manual/gamut_masks.rst:53
msgid ":ref:`gamut_mask_docker`"
msgstr ":ref:`gamut_mask_docker`"

#: ../../user_manual/gamut_masks.rst:57
msgid "In the color selector"
msgstr "Al selector de color"

#: ../../user_manual/gamut_masks.rst:59
msgid ""
"You can rotate an existing mask directly in the color selector, by dragging "
"the rotation slider on top of the selector (2)."
msgstr ""
"Podeu fer girar una màscara existent directament al selector de color, "
"arrossegant el control lliscant de gir que hi ha a la part superior del "
"selector (2)."

#: ../../user_manual/gamut_masks.rst:61
msgid ""
"The mask can be toggled off and on again by the toggle mask button in the "
"top left corner (1)."
msgstr ""
"La màscara es pot alternar entre activada i desactivada emprant el botó "
"d'alternar la màscara que hi ha a la cantonada superior esquerra (1)."

#: ../../user_manual/gamut_masks.rst:65
msgid ".. image:: images/dockers/GamutMasks_Selectors.png"
msgstr ".. image:: images/dockers/GamutMasks_Selectors.png"

#: ../../user_manual/gamut_masks.rst:65
msgid "Advanced and Artistic color selectors with a gamut mask"
msgstr ""
"Selector avançat del color i Selector del color artístic amb una màscara de "
"gamma"

#: ../../user_manual/gamut_masks.rst:69
msgid ":ref:`artistic_color_selector_docker`"
msgstr ":ref:`artistic_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:70
msgid ":ref:`advanced_color_selector_docker`"
msgstr ":ref:`advanced_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:74
msgid "Editing/creating a custom gamut mask"
msgstr "Editar/crear una màscara de gamma personalitzada"

#: ../../user_manual/gamut_masks.rst:78
msgid ""
"To rotate a mask around the center point use the rotation slider in the "
"color selector."
msgstr ""
"Per a fer girar una màscara al voltant del punt central, utilitzeu el "
"control lliscant de gir que hi ha al selector de color."

#: ../../user_manual/gamut_masks.rst:80
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template documents open as a new view (1)."
msgstr ""
"Si trieu crear una màscara nova, editar o duplicar la màscara seleccionada, "
"la plantilla de màscara s'obrirà a una vista nova (1)."

# skip-rule: punctuation-period,t-acc_obe
#: ../../user_manual/gamut_masks.rst:82
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`). Please note, that the mask is intended to be "
"composed of basic vector shapes. Although interesting results might arise "
"from using advanced vector drawing techniques, not all features are "
"guaranteed to work properly (e.g. grouping, vector text, etc.)."
msgstr ""
"Allà podreu crear formes noves i modificar la màscara amb eines vectorials "
"estàndard (:ref:`vector_graphics`). Recordeu que la màscara es compon de "
"formes vectorials bàsiques. Encara que poden sorgir resultats interessants "
"fent ús de tècniques avançades del dibuix vectorial, no es garanteix que "
"funcionin correctament totes les característiques (p. ex., agrupació, text "
"vectorial, etc.)"

#: ../../user_manual/gamut_masks.rst:86
msgid ""
"Transformations done through the transform tool or layer transform cannot be "
"saved in a gamut mask. The thumbnail image reflects the changes, but the "
"individual mask shapes do not."
msgstr ""
"Les transformacions realitzades mitjançant l'eina de transformació o la "
"transformació de la capa no es poden desar en una màscara de gamma. La "
"imatge en miniatura reflectirà els canvis, però les formes de cada màscara "
"individual no ho farà."

#: ../../user_manual/gamut_masks.rst:88
msgid ""
"You can :guilabel:`Preview` the mask in the color selector (4). If you are "
"satisfied with your changes, :guilabel:`Save` the mask (5). :guilabel:"
"`Cancel` (3) will close the editing view without saving your changes."
msgstr ""
"Podeu fer una :guilabel:`Vista prèvia` de la màscara al selector de color "
"(4). Si esteu satisfet amb els vostres canvis, feu un :guilabel:`Desa` la "
"màscara (5). Feu un :guilabel:`Cancel·la` (3) tancarà la vista d'edició "
"sense desar els canvis."

#: ../../user_manual/gamut_masks.rst:92
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../user_manual/gamut_masks.rst:92
msgid "Editing a gamut mask"
msgstr "Editant una màscara de gamma"

#: ../../user_manual/gamut_masks.rst:96
msgid "Importing and exporting"
msgstr "Importar i exportar"

#: ../../user_manual/gamut_masks.rst:98
msgid ""
"Gamut masks can be imported and exported in bundles in the Resource Manager. "
"See :ref:`resource_management` for more information."
msgstr ""
"Les màscares de gamma es poden importar i exportar en paquets al Gestor de "
"recursos. Per a més informació, vegeu :ref:`resource_management`."
