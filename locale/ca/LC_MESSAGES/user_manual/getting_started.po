# Translation of docs_krita_org_user_manual___getting_started.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-20 14:38+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "Com començar"

#: ../../user_manual/getting_started.rst:7
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr ""
"Us donem la benvinguda al manual del Krita! En aquesta secció intentarem que "
"aneu més ràpid."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/getting_started.rst:9
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""
"Si esteu familiaritzat amb la pintura digital, us recomanem que doneu una "
"ullada a la categoria :ref:`introduction_from_other_software`, la qual conté "
"guies que us ajudaran a familiaritzar-vos amb el Krita, comparant les seves "
"funcions amb altre programari."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/getting_started.rst:11
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""
"Si sou nou en l'art digital, simplement comenceu amb :ref:`installation`, la "
"qual tracta sobre com instal·lar el Krita i continueu amb :ref:"
"`starting_with_krita`, la qual ajuda a crear un document nou i desar-lo, "
"els :ref:`basic_concepts`, on intentarem cobrir ràpidament les grans "
"categories de funcionalitats del Krita, i finalment la :ref:`navigation`, la "
"qual us ajudarà a trobar ajuda en l'ús bàsic, com ara el desplaçament, el "
"zoom i el gir."

# skip-rule: t-sp_2p,t-2p_sp,t-acc_obe
#: ../../user_manual/getting_started.rst:13
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""
"Quan ho tingueu dominat, podreu consultar les pàgines dedicades a la "
"introducció per a la funcionalitat a la pàgina :ref:`user_manual`, llegiu a "
"través dels conceptes generals que hi ha al darrere de la pintura (digital) "
"a la secció :ref:`general_concepts` o simplement cerqueu al :ref:"
"`reference_manual` pel que fa a un botó específic."

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "Contingut:"
