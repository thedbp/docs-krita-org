# Translation of docs_krita_org_reference_manual___main_menu___window_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___window_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:34+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "List of open documents."
msgstr "Список відкритих документів."

#: ../../reference_manual/main_menu/window_menu.rst:1
msgid "The window menu in Krita."
msgstr "Меню «Вікно» у Krita."

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "Window"
msgstr "Вікно"

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "View"
msgstr "Перегляд"

#: ../../reference_manual/main_menu/window_menu.rst:16
msgid "Window Menu"
msgstr "Меню «Вікно»"

#: ../../reference_manual/main_menu/window_menu.rst:18
msgid "A menu completely dedicated to window management in Krita."
msgstr "Це меню у Krita повністю присвячено керуванню вікнами."

#: ../../reference_manual/main_menu/window_menu.rst:20
msgid "New Window"
msgstr "Нове вікно"

#: ../../reference_manual/main_menu/window_menu.rst:21
msgid "Creates a new window for Krita. Useful with multiple screens."
msgstr ""
"Створює нове вікно Krita. Корисно для режиму роботи із декількома екранами."

#: ../../reference_manual/main_menu/window_menu.rst:22
msgid "New View"
msgstr "Нова панель перегляду"

#: ../../reference_manual/main_menu/window_menu.rst:23
msgid ""
"Make a new view of the given document. You can have different zoom or "
"rotation on these."
msgstr ""
"Створити нову панель перегляду для вказаного документа. Для кожної панелі "
"можна встановлювати власний масштаб та обертання."

#: ../../reference_manual/main_menu/window_menu.rst:24
msgid "Workspace"
msgstr "Робочий простір"

#: ../../reference_manual/main_menu/window_menu.rst:25
msgid "A convenient access panel to the :ref:`resource_workspaces`."
msgstr "Зручна панель доступу до засобів :ref:`resource_workspaces`."

#: ../../reference_manual/main_menu/window_menu.rst:26
msgid "Close"
msgstr "Закрити"

#: ../../reference_manual/main_menu/window_menu.rst:27
msgid "Close the current view."
msgstr "Закрити поточну панель перегляду."

#: ../../reference_manual/main_menu/window_menu.rst:28
msgid "Close All"
msgstr "Закрити усе"

#: ../../reference_manual/main_menu/window_menu.rst:29
msgid "Close all documents."
msgstr "Закрити всі документи."

#: ../../reference_manual/main_menu/window_menu.rst:30
msgid "Tile"
msgstr "Мозаїка"

#: ../../reference_manual/main_menu/window_menu.rst:31
msgid "Tiles all open documents into a little sub-window."
msgstr ""
"Створити мозаїку з усіх відкритих документів, кожен у власному маленькому "
"віконці."

#: ../../reference_manual/main_menu/window_menu.rst:32
msgid "Cascade"
msgstr "Уступами"

#: ../../reference_manual/main_menu/window_menu.rst:33
msgid "Cascades the sub-windows."
msgstr "Створити каскад з підвікон."

#: ../../reference_manual/main_menu/window_menu.rst:34
msgid "Next"
msgstr "Далі"

#: ../../reference_manual/main_menu/window_menu.rst:35
msgid "Selects the next view."
msgstr "Вибрати наступну панель перегляду."

#: ../../reference_manual/main_menu/window_menu.rst:36
msgid "Previous"
msgstr "Назад"

#: ../../reference_manual/main_menu/window_menu.rst:37
msgid "Selects the previous view."
msgstr "Вибрати попередню панель перегляду."

#: ../../reference_manual/main_menu/window_menu.rst:39
msgid "Use this to switch between documents."
msgstr "Цим пунктом можна скористатися для перемикання між документами."
