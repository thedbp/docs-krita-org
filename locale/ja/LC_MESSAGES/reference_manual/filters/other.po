msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/filters/other.rst:1
msgid "Overview of the other filters."
msgstr ""

#: ../../reference_manual/filters/other.rst:15
msgid "Other"
msgstr ""

#: ../../reference_manual/filters/other.rst:17
msgid "Filters signified by them not fitting anywhere else."
msgstr ""

#: ../../reference_manual/filters/other.rst:20
msgid "Wave"
msgstr ""

#: ../../reference_manual/filters/other.rst:22
msgid "Adds a cute little wave-distortion effect to the input image."
msgstr ""

#: ../../reference_manual/filters/other.rst:24
#: ../../reference_manual/filters/other.rst:27
msgid "Random Noise"
msgstr ""

#: ../../reference_manual/filters/other.rst:29
msgid "Gives Random Noise to input image."
msgstr ""

#: ../../reference_manual/filters/other.rst:32
msgid "Random Pick"
msgstr ""

#: ../../reference_manual/filters/other.rst:34
msgid "Adds a little pixely-fringe to the input image."
msgstr ""
