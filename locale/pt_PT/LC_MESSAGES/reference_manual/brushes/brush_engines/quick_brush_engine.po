# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-20 17:20+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: optionsize Phabricator icons image quickbrush\n"
"X-POFile-SpellExtra: optionspacing blendingmodes images demãos ref\n"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:1
msgid "The Quick Brush Engine manual page."
msgstr "A página de anual do Motor do Pincel Rápido."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:15
msgid "Quick Brush Engine"
msgstr "Motor do Pincel Rápido"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:19
msgid ".. image:: images/icons/quickbrush.svg"
msgstr ".. image:: images/icons/quickbrush.svg"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:20
msgid ""
"A Brush Engine inspired by the common artist's workflow where a simple big "
"brush, like a marker, is used to fill large areas quickly, the Quick Brush "
"engine is an extremely simple, but quick brush, which can give the best "
"performance of all Brush Engines."
msgstr ""
"Um Motor de Pincel inspirado pelo fluxo dos artistas comuns, onde é usado um "
"pincel simples e grande, como um marcador, para preencher áreas grandes de "
"forma rápida - o motor do Pincel Rápido é um pincel extremamente simples mas "
"rápido que lhe poderá dar a melhor performance de todos os motores de "
"pincéis."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:22
msgid ""
"It can only change size, blending mode and spacing, and this allows for "
"making big optimisations that aren't possible with other brush engines."
msgstr ""
"Só é possível mudar o tamanho, o modo de mistura e o intervalo, o que "
"permite fazer grandes optimizações que não são possíveis com outros motores "
"de pincéis."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:26
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:27
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:30
msgid "Brush"
msgstr "Pincel"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:32
msgid "The only parameter specific to this brush."
msgstr "O único parâmetro específico deste pincel."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:34
msgid "Diameter"
msgstr "Diâmetro"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:35
msgid ""
"The size. This brush engine can only make round dabs, but it can make them "
"really fast despite size."
msgstr ""
"O tamanho. Este motor de pincel só consegue aplicar demãos redondas, embora "
"consiga fazê-las bastante rapidamente e independentemente do tamanho."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid "Spacing"
msgstr "Espaço"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid ""
"The spacing between the dabs. This brush engine is particular in that it's "
"faster with a lower spacing, unlike all other brush engines."
msgstr ""
"O espaço de intervalo entre as demãos. Este motor de pincel é particular na "
"medida em que é mais rápido com um espaço mais reduzido, ao contrário dos "
"outros motores de pincéis."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:40
msgid "`Phabricator Task <https://phabricator.kde.org/T3492>`_"
msgstr "`Tarefa do Phabricator <https://phabricator.kde.org/T3492>`_"
