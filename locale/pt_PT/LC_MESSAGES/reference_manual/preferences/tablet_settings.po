# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:28+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita ref image Ink Wacom Surface images WinTab\n"
"X-POFile-SpellExtra: Revoy KritaPreferencesTabletSettings Trig tablet\n"
"X-POFile-SpellExtra: preferences advanced settings recalibrar Tablet kbd\n"
"X-POFile-SpellExtra: Tabletes Preference Clicks Utility\n"

#: ../../reference_manual/preferences/tablet_settings.rst:1
msgid "Configuring the tablet in Krita."
msgstr "Configuração da tablete no Krita."

#: ../../reference_manual/preferences/tablet_settings.rst:12
#: ../../reference_manual/preferences/tablet_settings.rst:21
msgid "Tablet"
msgstr "Tablete"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Pressure Curve"
msgstr "Curva de Pressão"

#: ../../reference_manual/preferences/tablet_settings.rst:17
msgid "Tablet Settings"
msgstr "Configuração da Tablete"

#: ../../reference_manual/preferences/tablet_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"

#: ../../reference_manual/preferences/tablet_settings.rst:22
msgid ""
"Input Pressure Global Curve : This is the global curve setting that your "
"tablet will use in Krita. The settings here will make your tablet feel soft "
"or hard globally."
msgstr ""
"Curva Global de Pressão à Entrada : Esta é a configuração global da curva "
"que a sua tablete irá usar no Krita. As definições aqui farão com que a sua "
"tablete pareça mais leve ou pesada a nível global."

#: ../../reference_manual/preferences/tablet_settings.rst:24
msgid ""
"Some tablet devices don't tell us whether the side buttons on a stylus. If "
"you have such a device, you can try activate this workaround. Krita will try "
"to read right and middle-button clicks as if they were coming from a mouse "
"instead of a tablet. It may or may not work on your device (depends on the "
"tablet driver implementation). After changing this option Krita should be "
"restarted."
msgstr ""
"Alguns dispositivos de tablete não nos comunicam quais são os botões "
"laterais de um lápis. Se tiver um desses dispositivos, poderá tentar activar "
"este truque. O Krita irá tentar ler os 'clicks' do botão direito e do mesmo "
"como se estivessem a vir de um rato em vez de uma tablete. Poderá ou não "
"funcionar no seu dispositivo (depende da implementação do controlador da "
"tablete). Depois de mudar esta opção, deverá reiniciar o Krita."

#: ../../reference_manual/preferences/tablet_settings.rst:26
msgid "Use Mouse Events for Right and Middle clicks."
msgstr "Usar os Eventos do Rato para os 'Clicks' Direitos e do Meio."

#: ../../reference_manual/preferences/tablet_settings.rst:29
msgid "On Windows 8 or above only."
msgstr "Apenas no Windows 8 ou superior."

#: ../../reference_manual/preferences/tablet_settings.rst:31
msgid "WinTab"
msgstr "WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:32
msgid ""
"Use the WinTab API to receive tablet pen input. This is the API being used "
"before Krita 3.3. This option is recommended for most Wacom tablets."
msgstr ""
"Use a API do WinTab para receber dados introduzidos pela tablete. Esta é a "
"API a ser usada antes do Krita 3.3. Esta opção é recomendada para a maioria "
"das tabletes da Wacom."

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "For Krita 3.3 or later:Tablet Input API"
msgstr "Para o Krita 3.3 ou posterior: API de Entrada da Tablete"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "Windows 8+ Pointer Input"
msgstr "Introdução do Ponteiro do Windows 8+"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid ""
"Use the Pointer Input messages to receive tablet pen input. This option "
"depends on Windows Ink support from the tablet driver. This is a relatively "
"new addition so it's still considered to be experimental, but it should work "
"well enough for painting. You should try this if you are using an N-Trig "
"device (e.g. recent Microsoft Surface devices) or if your tablet does not "
"work well with WinTab."
msgstr ""
"Use as mensagens de Entrada do Ponteiro para receber os dados de entrada da "
"caneta da tablete. Esta opção depende do suporte por parte do controlador da "
"tablete para o Windows Ink. Esta é uma funcionalidade relativamente nova, "
"pelo que se considera ser experimental, mas deverá funcionar suficientemente "
"bem para a pintura. Deverá tentar isto se estiver a usar um dispositivo N-"
"Trig (p.ex. os dispositivos Microsoft Surface recentes) ou se a sua tablete "
"não funcionar bem com o WinTab."

#: ../../reference_manual/preferences/tablet_settings.rst:37
msgid "Advanced Tablet Settings for WinTab"
msgstr "Configuração Avançada da Tablete no WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:41
msgid ".. image:: images/preferences/advanced-settings-tablet.png"
msgstr ".. image:: images/preferences/advanced-settings-tablet.png"

#: ../../reference_manual/preferences/tablet_settings.rst:42
msgid ""
"When using multiple monitors or using a tablet that is also a screen, Krita "
"will get conflicting information about how big your screen is, and sometimes "
"if it has to choose itself, there will be a tablet offset. This window "
"allows you to select the appropriate screen resolution."
msgstr ""
"Ao usar vários monitores ou usar uma tablete que também é um ecrã, o Krita "
"irá obter informações em conflito sobre o tamanho do seu ecrã e, em alguns "
"casos se tiver de escolher ele mesmo, irá existir um deslocamento da "
"tablete. Esta janela permite-lhe seleccionar a resolução apropriada do ecrã."

#: ../../reference_manual/preferences/tablet_settings.rst:44
msgid "Use Information Provided by Tablet"
msgstr "Usar a Informação Devolvida pela Tablete"

#: ../../reference_manual/preferences/tablet_settings.rst:45
msgid "Use the information as given by the tablet."
msgstr "Usa a informação que é fornecida pela tablete."

#: ../../reference_manual/preferences/tablet_settings.rst:46
msgid "Map to entire virtual screen"
msgstr "Associar ao ecrã virtual inteiro"

#: ../../reference_manual/preferences/tablet_settings.rst:47
msgid "Use the information as given by Windows."
msgstr "Usa a informação devolvida pelo Windows."

#: ../../reference_manual/preferences/tablet_settings.rst:49
msgid ""
"Type in the numbers manually. Use this when you have tried the other "
"options. You might even need to do trial and error if that is the case, but "
"at the least you can configure it."
msgstr ""
"Indique os números manualmente. Use isto quando tiver experimentado as "
"outras opções. Poderá até usar a tentativa e erro, se for o caso; porém, o "
"mínimo será configurá-lo."

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid "Map to Custom Area"
msgstr "Associar a uma Área Personalizada"

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid ""
"If you have a dual monitor setup and only the top half of the screen is "
"reachable, you might have to enter the total width of both screens plus the "
"double height of your monitor in this field."
msgstr ""
"Se tiver uma configuração com dois monitores e só a metade superior do ecrã "
"estiver acessível, provavelmente terá de indicar a largura total de ambos os "
"ecrãs mais o dobro da altura do seu monitor neste campo."

#: ../../reference_manual/preferences/tablet_settings.rst:55
msgid ""
"To access this dialog in Krita versions older than 4.2, you had to do the "
"following:"
msgstr ""
"Para aceder a esta janela nas versões do Krita anteriores à 4.2, terá de "
"fazer o seguinte:"

#: ../../reference_manual/preferences/tablet_settings.rst:57
msgid "Put your stylus away from the tablet."
msgstr "Coloque o seu lápis longe da tablete."

#: ../../reference_manual/preferences/tablet_settings.rst:58
msgid ""
"Start Krita without using a stylus, that is using a mouse or a keyboard."
msgstr "Inicie o Krita sem usar o lápis; isto é, usando um rato ou o teclado."

#: ../../reference_manual/preferences/tablet_settings.rst:59
msgid "Press the :kbd:`Shift` key and hold it."
msgstr "Carregue no :kbd:`Shift` e mantenha-o assim carregado."

#: ../../reference_manual/preferences/tablet_settings.rst:60
msgid "Touch a tablet with your stylus so Krita would recognize it."
msgstr "Toque numa tablete com o seu lápis para que o Krita o reconheça."

#: ../../reference_manual/preferences/tablet_settings.rst:62
msgid ""
"If adjusting this doesn't work, and if you have a Wacom tablet, an offset in "
"the canvas can be caused by a faulty Wacom preference file which is not "
"removed or replaced by reinstalling the drivers."
msgstr ""
"Se ajustar isto não resultar, e caso tenha uma tablete da Wacom, o "
"deslocamento na área de desenho poderá ser provocado por um ficheiro de "
"preferências da Wacom com problemas e que não é removido ou substituído sem "
"reinstalar os controladores."

#: ../../reference_manual/preferences/tablet_settings.rst:64
msgid ""
"To fix it, use the “Wacom Tablet Preference File Utility” to clear all the "
"preferences. This should allow Krita to detect the correct settings "
"automatically."
msgstr ""
"Para o corrigir, use o “Wacom Tablet Preference File Utility” (Utilitário de "
"Ficheiros de Preferências de Tabletes Wacom) para limpar todas as "
"preferências. Isto deverá permitir ao Krita detectar as configurações "
"correctas de forma automática."

#: ../../reference_manual/preferences/tablet_settings.rst:67
msgid ""
"Clearing all wacom preferences will reset your tablet's configuration, thus "
"you will need to recalibrate/reconfigure it."
msgstr ""
"Se limpar todas as preferências da Wacom, irá limpar a configuração da sua "
"tablete, pelo que terá à mesma de recalibrar/reconfigurar a mesma."

#: ../../reference_manual/preferences/tablet_settings.rst:70
msgid "Tablet Tester"
msgstr "Teste da Tablete"

#: ../../reference_manual/preferences/tablet_settings.rst:74
msgid ""
"This is a special feature for debugging tablet input. When you click on it, "
"it will open a window with two sections. The left section is the **Drawing "
"Area** and the right is the **Text Output**."
msgstr ""
"Esta é uma funcionalidade especial para a depuração dos dados introduzidos "
"pela tablete. Quando carregar nela, irá abrir uma janela com duas secções. A "
"secção esquerda é a **Área de Desenho** e a da direita é o **Resultado de "
"Texto**."

#: ../../reference_manual/preferences/tablet_settings.rst:76
msgid ""
"If you draw over the Drawing Area, you will see a line appear. If your "
"tablet is working it should be both a red and blue line."
msgstr ""
"Se desenhar sobre a Área de Desenho, irá ver uma linha a aparecer. Se a sua "
"tablete estiver funcional, deverá ser tanto uma linha vermelha como uma azul."

#: ../../reference_manual/preferences/tablet_settings.rst:78
msgid ""
"The red line represents mouse events. Mouse events are the most basic events "
"that Krita can pick up. However, mouse events have crude coordinates and "
"have no pressure sensitivity."
msgstr ""
"A linha vermelha representa os eventos do rato. Os eventos do rato são os "
"eventos mais básicos que o Krita consegue recolher. Contudo, os eventos do "
"rato têm coordenadas pouco precisas e não têm sensibilidade à pressão."

#: ../../reference_manual/preferences/tablet_settings.rst:80
msgid ""
"The blue line represents the tablet events. The tablet events only show up "
"when Krita can access your tablet. These have more precise coordinates and "
"access to sensors like pressure sensitivity."
msgstr ""
"A linha azul representa os eventos da tablete. Os eventos da tablete só "
"aparecem quando o Krita consegue aceder à sua tablete. Estes têm coordenadas "
"com maior precisão e acesso a sensores como o de sensibilidade da pressão."

#: ../../reference_manual/preferences/tablet_settings.rst:84
msgid ""
"If you have no blue line when drawing on the lefthand drawing area, Krita "
"cannot access your tablet. Check out the :ref:`page on drawing tablets "
"<drawing_tablets>` for suggestions on what is causing this."
msgstr ""
"Se não tiver nenhuma linha azul ao desenhar sobre a área de desenho à "
"esquerda, o Krita não conseguirá aceder à sua tablete. Verifique a :ref:"
"`página sobre as tabletes de desenho <drawing_tablets>` para obter algumas "
"sugestões sobre o que poderá causar isto."

#: ../../reference_manual/preferences/tablet_settings.rst:86
msgid ""
"When you draw a line, the output on the right will show all sorts of text "
"output. This text output can be attached to a help request or a bug report "
"to figure out what is going on."
msgstr ""
"Quando desenhar uma linha, o resultado à direita irá mostrar todos os tipos "
"de resultados em texto. Estes poderão ser anexados a um pedido de ajuda ou a "
"um relatório de erros para descobrir o que se passa."

#: ../../reference_manual/preferences/tablet_settings.rst:89
msgid "External Links"
msgstr "Referências Externas"

#: ../../reference_manual/preferences/tablet_settings.rst:91
msgid ""
"`David Revoy wrote an indepth guide on using this feature to maximum "
"advantage. <https://www.davidrevoy.com/article182/calibrating-wacom-stylus-"
"pressure-on-krita>`_"
msgstr ""
"`O David Revoy escreveu um guia aprofundado sobre o uso desta funcionalidade "
"ao máximo. <https://www.davidrevoy.com/article182/calibrating-wacom-stylus-"
"pressure-on-krita>`_"
