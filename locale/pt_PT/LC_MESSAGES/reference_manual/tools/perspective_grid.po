# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 18:05+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Multigrid en perspectivegridtool image icons\n"
"X-POFile-SpellExtra: menuselection KritaPersgridnoedit images\n"
"X-POFile-SpellExtra: Perspectivegrid alt toolperspectivegrid ref\n"
"X-POFile-SpellExtra: assistantperspective tools\n"

#: ../../<rst_epilog>:50
msgid ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"
msgstr ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: ferramenta da grelha de perspectiva"

#: ../../reference_manual/tools/perspective_grid.rst:1
msgid "Krita's perspective grid tool reference."
msgstr "A referência da ferramenta da grelha de perspectiva."

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Perspective"
msgstr "Perspectiva"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Grid"
msgstr "Grelha"

#: ../../reference_manual/tools/perspective_grid.rst:16
msgid "Perspective Grid Tool"
msgstr "Ferramenta da Grelha de Perspectiva"

#: ../../reference_manual/tools/perspective_grid.rst:18
msgid "|toolperspectivegrid|"
msgstr "|toolperspectivegrid|"

#: ../../reference_manual/tools/perspective_grid.rst:22
msgid "Deprecated in 3.0, use the :ref:`assistant_perspective` instead."
msgstr ""
"Descontinuada no 3.0; use a :ref:`assistant_perspective` como alternativa."

#: ../../reference_manual/tools/perspective_grid.rst:24
msgid ""
"The perspective grid tool allows you to draw and manipulate grids on the "
"canvas that can serve as perspective guides for your painting. A grid can be "
"added to your canvas by first clicking the tool in the tool bar and then "
"clicking four points on the canvas which will serve as the four corners of "
"your grid."
msgstr ""
"A ferramenta da grelha de perspectiva permite-lhe desenhar e manipular as "
"grelhas na área de desenho que possam servir como guias de perspectiva para "
"a sua pintura. Poderá adicionar uma grelha à sua área de desenho se carregar "
"primeiro na ferramenta da barra de ferramentas e depois carregar em quatro "
"pontos da área de desenho que servirão como os quatro cantos da sua grelha."

#: ../../reference_manual/tools/perspective_grid.rst:27
msgid ".. image:: images/tools/Perspectivegrid.png"
msgstr ".. image:: images/tools/Perspectivegrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:28
msgid ""
"The grid can be manipulated by pulling on any of its four corners. The grid "
"can be extended by clicking and dragging a midpoint of one of its edges. "
"This will allow you to expand the grid at other angles. This process can be "
"repeated on any subsequent grid or grid section. You can join the corners of "
"two grids by dragging one onto the other. Once they are joined they will "
"always move together, they cannot be separated. You can delete any grid by "
"clicking on the red X at its center. This tool can be used to build "
"reference for complex scenes."
msgstr ""
"A grelha poderá ser manipulada se arrastar qualquer um dos quatro cantos. A "
"grelha poderá ser estendida se carregar e arrastar um ponto intermédio num "
"dos seus extremos. Isto permitir-lhe-á expandir a grelha para quaisquer "
"outros ângulos. Este processo pode ser repetido em qualquer grelha ou secção "
"de grelha subsequente. Poderá juntar os cantos de duas grelhas se arrastar "
"uma para a outra. Assim que estiverem juntas, podê-las-á mover em conjunto, "
"sem que as mesmas se separem. Poderá apagar qualquer grelha se carregar no X "
"vermelho ao seu centro. Esta ferramenta pode ser usada para criar uma "
"referência para cenas complexas."

#: ../../reference_manual/tools/perspective_grid.rst:30
msgid "As displayed while the Perspective Grid tool is active: *"
msgstr "Como é apresentada com a ferramenta da Grelha de Perspectiva activa: *"

#: ../../reference_manual/tools/perspective_grid.rst:33
msgid ".. image:: images/tools/Multigrid.png"
msgstr ".. image:: images/tools/Multigrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:34
msgid "As displayed while any other tool is active: *"
msgstr "Como é apresentada com qualquer outra ferramenta activa: *"

#: ../../reference_manual/tools/perspective_grid.rst:37
msgid ".. image:: images/tools/KritaPersgridnoedit.png"
msgstr ".. image:: images/tools/KritaPersgridnoedit.png"

#: ../../reference_manual/tools/perspective_grid.rst:38
msgid ""
"You can toggle the visibility of the grid from the main menu :menuselection:"
"`View --> Show Perspective Grid` option. You can also clear any grid setup "
"you have and start over by using the :menuselection:`View --> Clear "
"Perspective Grid`."
msgstr ""
"Poderá activar ou desactivar a visibilidade da grelha a partir do menu "
"principal :menuselection:`Ver --> Mostrar a Grelha da Perspectiva`. Poderá "
"também limpar qualquer configuração da grelha que tenha e começar de novo "
"com a opção :menuselection:`Ver --> Limpar a Grelha da Perspectiva`."
