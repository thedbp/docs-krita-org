msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 14:03+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/main_menu.rst:5
msgid "Main Menu"
msgstr "Menu Principal"

#: ../../reference_manual/main_menu.rst:7
msgid ""
"A list of all of main menu actions and a short description on what they do."
msgstr ""
"Uma lista com todas as acções principais do menu, bem como uma breve "
"descrição do que fazem."
