# translation of docs_krita_org_reference_manual___tools___dyna.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___dyna\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 13:11+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:38
msgid ""
".. image:: images/icons/dyna_tool.svg\n"
"   :alt: tooldyna"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:1
msgid "Krita's dynamic brush tool reference."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:11
msgid "Dyna"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:16
msgid "Dynamic Brush Tool"
msgstr "Nástroj dynamického štetca"

#: ../../reference_manual/tools/dyna.rst:18
msgid "|tooldyna|"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:20
msgid ""
"Add custom smoothing dynamics to your brush. This will give you similar "
"smoothing results as the normal freehand brush. There are a couple options "
"that you can change."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:22
msgid "Mass"
msgstr "Hmotnosť"

#: ../../reference_manual/tools/dyna.rst:23
msgid ""
"Average your movement to make it appear smoother. Higher values will make "
"your brush move slower."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:25
msgid "Drag"
msgstr "Chyť"

#: ../../reference_manual/tools/dyna.rst:25
msgid ""
"A rubberband effect that will help your lines come back to your cursor. "
"Lower values will make the effect more extreme."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:27
msgid "Recommended values are around 0.02 Mass and 0.92 Drag."
msgstr ""
