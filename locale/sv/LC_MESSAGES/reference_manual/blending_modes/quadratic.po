# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-30 15:30+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/blending_modes/quadratic.rst:1
msgid ""
"Page about the quadratic blending modes in Krita: Freeze, Freeze-Reflect, "
"Glow, Glow-Heat, Heat, Heat-Glow, Heat-Glow/Freeze-Reflect Hybrid, Reflect "
"and Reflect-Freeze."
msgstr ""
"Sida om de kvadratiska blandningslägena i Krita: Frys, Frys-reflektera, "
"Glöd, Glöd-hetta, Hetta, Hetta-glöd, Hybrid av hetta-glöd och frys-"
"reflektera, Reflektera och Reflektera-frys."

#: ../../reference_manual/blending_modes/quadratic.rst:11
#: ../../reference_manual/blending_modes/quadratic.rst:15
msgid "Quadratic"
msgstr "Kvadratisk"

#: ../../reference_manual/blending_modes/quadratic.rst:19
msgid ""
"The quadratic blending modes are a set of modes intended to give various "
"effects when adding light zones or overlaying shiny objects."
msgstr ""
"De kvadratiska blandningslägena är en uppsättning lägen avsedda att ge "
"diverse effekter när ljuszoner eller överlagrade blanka objekt adderas."

#: ../../reference_manual/blending_modes/quadratic.rst:23
msgid "Freeze Blending Mode"
msgstr "Frysblandningslägen"

#: ../../reference_manual/blending_modes/quadratic.rst:26
msgid "Freeze"
msgstr "Frys"

#: ../../reference_manual/blending_modes/quadratic.rst:28
msgid "The freeze blending mode. Inversion of the reflect blending mode."
msgstr "Blandningsläget Frys. Invertering av av blandningsläget Reflektera."

#: ../../reference_manual/blending_modes/quadratic.rst:33
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:33
msgid "Left: **Normal**. Right: **Freeze**."
msgstr "Vänster: **Normal**. Höger: **Frys**."

#: ../../reference_manual/blending_modes/quadratic.rst:39
msgid "Freeze-Reflect"
msgstr "Frys-reflektera"

#: ../../reference_manual/blending_modes/quadratic.rst:41
msgid "Mix of Freeze and Reflect blend mode."
msgstr "Mix av blandningslägena Frys och Reflektera."

#: ../../reference_manual/blending_modes/quadratic.rst:46
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Reflect_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Reflect_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:46
msgid "Left: **Normal**. Right: **Freeze-Reflect**."
msgstr "Vänster: **Normal**. Höger: **Frys-reflektera**."

#: ../../reference_manual/blending_modes/quadratic.rst:51
msgid "Glow"
msgstr "Glöd"

#: ../../reference_manual/blending_modes/quadratic.rst:53
msgid "Reflect Blend Mode with source and destination layers swapped."
msgstr "Blandningsläget Reflektera med käll- och mållagret utbytta."

#: ../../reference_manual/blending_modes/quadratic.rst:58
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:58
msgid "Left: **Normal**. Right: **Glow**."
msgstr "Vänster: **Normal**. Höger: **Glöd**."

#: ../../reference_manual/blending_modes/quadratic.rst:63
msgid "Glow-Heat"
msgstr "Glöd-hetta"

#: ../../reference_manual/blending_modes/quadratic.rst:65
msgid "Mix of Glow and Heat blend mode."
msgstr "Mix av blandningslägena Glöd och Hetta."

#: ../../reference_manual/blending_modes/quadratic.rst:70
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Heat_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Heat_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:70
msgid "Left: **Normal**. Right: **Glow_Heat**."
msgstr "Vänster: **Normal**. Höger: **Glöd-hetta**."

#: ../../reference_manual/blending_modes/quadratic.rst:75
msgid "Heat"
msgstr "Hetta"

#: ../../reference_manual/blending_modes/quadratic.rst:77
msgid "The Heat Blend Mode. Inversion of the Glow Blend Mode."
msgstr "Blandningsläget Hetta. Invertering av av blandningsläget Glöd."

#: ../../reference_manual/blending_modes/quadratic.rst:83
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:83
msgid "Left: **Normal**. Right: **Heat**."
msgstr "Vänster: **Normal**. Höger: **Hetta**."

#: ../../reference_manual/blending_modes/quadratic.rst:88
msgid "Heat-Glow"
msgstr "Hetta-glöd"

#: ../../reference_manual/blending_modes/quadratic.rst:90
msgid "Mix of Heat, and Glow blending mode."
msgstr "Mix av blandningslägena Hetta och Glöd."

#: ../../reference_manual/blending_modes/quadratic.rst:95
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:95
msgid "Left: **Normal**. Right: **Heat-Glow**."
msgstr "Vänster: **Normal**. Höger: **Hetta-glöd**."

#: ../../reference_manual/blending_modes/quadratic.rst:100
msgid "Heat-Glow and Freeze-Reflect Hybrid"
msgstr "Hybrid av hetta-glöd och frys-reflektera"

#: ../../reference_manual/blending_modes/quadratic.rst:102
msgid ""
"Mix of the continuous quadratic blending modes. Very similar to overlay, and "
"sometimes provides better result than overlay."
msgstr ""
"Mix av de kontinuerliga kvadratiska blandningslägen. Mycket liknande "
"överlagring, och ger ibland bättre resultat än överlagring."

#: ../../reference_manual/blending_modes/quadratic.rst:107
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Freeze_Reflect_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Freeze_Reflect_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:107
msgid "Left: **Normal**. Right: **Heat-Glow and Freeze-Reflect Hybrid**."
msgstr ""
"Vänster: **Normal**. Höger: **Hybrid av hetta-glöd och frys-reflektera**."

#: ../../reference_manual/blending_modes/quadratic.rst:112
msgid "Reflect"
msgstr "Reflektera"

#: ../../reference_manual/blending_modes/quadratic.rst:114
msgid ""
"Reflect is essentially Color Dodge Blending mode with quadratic falloff."
msgstr ""
"Reflektera är egentligen blandningsläget Färgblekning med kvadratisk "
"minskning."

#: ../../reference_manual/blending_modes/quadratic.rst:120
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:120
msgid "Left: **Normal**. Right: **Reflect**."
msgstr "Vänster: **Normal**. Höger: **Reflektera**."

#: ../../reference_manual/blending_modes/quadratic.rst:125
msgid "Reflect-Freeze"
msgstr "Reflektera-frys"

#: ../../reference_manual/blending_modes/quadratic.rst:127
msgid "Mix of Reflect and Freeze blend mode."
msgstr "Mix av blandningslägena Reflektera och Frys."

#: ../../reference_manual/blending_modes/quadratic.rst:132
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Freeze_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Freeze_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:132
msgid "Left: **Normal**. Right: **Reflect-Freeze**."
msgstr "Vänster: **Normal**. Höger: **Reflektera-frys**."
