# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
# Lo Fernchu <lofernchu@zonnet.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:17+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats.rst:1
msgid "The file formats category."
msgstr "De categorie bestandsformaten."

#: ../../general_concepts/file_formats.rst:14
msgid "File Formats"
msgstr "Bestandsformaten"

#: ../../general_concepts/file_formats.rst:16
msgid ""
"This category is for graphics file-formats. While most file-formats can be "
"looked up on wikipedia, this doesn't always explain what the format can be "
"used for and what its strengths and weaknesses are."
msgstr ""
"Deze categorie gaat over grafische bestandsformaten. Ofschoon u de meeste "
"bestandsformaten kunt opzoeken in wikipedia, is daar meestal geen uitleg "
"waarvoor u het bestandsformaat kunt gebruiken en wat de sterke en zwakke "
"punten zijn."

#: ../../general_concepts/file_formats.rst:18
msgid ""
"In this category we try to describe these in a manner that can be read by "
"beginners."
msgstr ""
"In deze categorie proberen we het zo te beschrijven dat ook een beginneling "
"het begrijpt."

#: ../../general_concepts/file_formats.rst:20
msgid ""
"Generally, there are the following features that people pay attention to in "
"regards to file formats:"
msgstr ""
"In het algemeen zijn er de volgende kenmerken waar mensen op letten wat "
"betreft bestandsformaten:"

#: ../../general_concepts/file_formats.rst:23
msgid "Compression"
msgstr "Compressie"

#: ../../general_concepts/file_formats.rst:25
msgid ""
"Compression is how the file-format tries to describe the image with as "
"little data as possible, so that the resulting file is as small as it can "
"get without losing quality."
msgstr ""
"Compressie is hoe het bestandsformaat probeert om de afbeelding met zo min "
"mogelijk data te beschrijven, zodat het resulterende bestand zo klein "
"mogelijk is zonder kwaliteit te verliezen."

#: ../../general_concepts/file_formats.rst:27
msgid ""
"What we generally see is that formats that are small on disk either lose "
"image quality, or require the computer to spend a lot of time thinking about "
"how the image should look."
msgstr ""
"Wat we meestal zien is dat bestandsformaten die weinig ruimte op de harde "
"schijf nodig hebben, of veel kwaliteit in de afbeelding verliezen of "
"vereisen dat de computer veel tijd moet spenderen om te bepalen hoe de "
"afbeelding eruit zou moeten zien."

#: ../../general_concepts/file_formats.rst:29
msgid ""
"Vector file-formats like ``SVG`` are a typical example of the latter. They "
"are really small because the technology used to create them is based on "
"mathematics, so it only stores maths-variables and can achieve very high "
"quality. The downside is that the computer needs to spend a lot of time "
"thinking about how it should look, and sometimes different programs have "
"different ways of interpreting the values. Furthermore, vector file-formats "
"imply vector graphics, which is a very different way of working than Krita "
"is specialized in."
msgstr ""
"Vector bestandsformaten zoals ``SVG`` zijn een goed voorbeeld van de "
"laatste. Ze zijn erg klein omdat de gebruikte technologie is gebaseerd op "
"wiskunde, het slaat daarom alleen wiskundige variabelen op en kan daarmee "
"een zeer goede kwaliteit bereiken. Het nadeel is dat het veel computertijd "
"kost om uit te rekenen hoe het eruit moet zien, en soms interpreteren "
"verschillende programma's de variabelen op een andere manier. Bovendien, "
"houden vector bestandsformaten in dat het vector afbeeldingen zijn, die op "
"een heel andere manier verwerkt worden dan waarin Krita gespecialiseerd is."

#: ../../general_concepts/file_formats.rst:31
msgid ""
":ref:`Lossy file formats <lossy_compression>`, like ``JPG`` or ``WebP`` are "
"an example of small on disk, but lowering the quality, and are best used for "
"very particular types of images. Lossy thus means that the file format plays "
"fast and loose with describing your image to reduce filesize."
msgstr ""
":ref:`bestandsformaten met verlies <lossy_compression>`, zoals ``JPG`` of "
"``WebP`` zijn een voorbeeld die weinig ruimte op de schijf nodig hebben, "
"maar wel de kwaliteit verminderen, en kunt u het beste alleen maar voor "
"bepaalde soorten afbeeldingen gebruiken. Lossy houd dus in dat het "
"bestandsformaat snel afgespeeld word maar ook informatie over uw afbeelding "
"vermindert om de bestandsgrootte te verkleinen."

#: ../../general_concepts/file_formats.rst:33
msgid ""
":ref:`Non-lossy or lossless formats <lossless_compression>`, like ``PNG``, "
"``GIF`` or ``BMP`` are in contrast, much heavier on disk, but much more "
"likely to retain quality."
msgstr ""
":ref:`Non-lossy of lossless formaten <lossless_compression>`, zoals ``PNG``, "
"``GIF`` of ``BMP`` hebben in contrast, veel meer ruimte nodig op de harde "
"schijf, maar bewaren waarschijnlijk veel meer details."

#: ../../general_concepts/file_formats.rst:35
msgid ""
"Then, there's proper working file formats like Krita's ``KRA``, Gimp's "
"``XCF``, Photoshop's ``PSD``, but also interchange formats like ``ORA`` and "
"``EXR``. These are the heaviest on the hard-drive and often require special "
"programs to open them up, but on the other hand these are meant to keep your "
"working environment intact, and keep all the layers and guides in them."
msgstr ""
"En dan zijn er de echte werkbare bestandsformaten zoals Krita's ``KRA``, "
"Gimp's ``XCF``, Photoshop's ``PSD``, maar ook de uitwisselingsformaten zoals "
"``ORA`` en ``EXR``. Deze nemen op de harde schijf de meeste ruimte in en u "
"heeft er meestal speciale programma's voor nodig om ze te kunnen bewerken, "
"maar ze zijn wel bedoelt om de werkomgeving in takt te laten, alle layers te "
"bewaren en te ordenen."

#: ../../general_concepts/file_formats.rst:38
msgid "Metadata"
msgstr "Metagegevens"

#: ../../general_concepts/file_formats.rst:40
msgid ""
"Metadata is the ability of a file format to contain information outside of "
"the actual image contents. This can be human readable data, like the date of "
"creation, the name of the author, a description of the image, but also "
"computer readable data, like an icc-profile which tells the computer about "
"the qualities of how the colors inside the file should be read."
msgstr ""
"Metadata is de mogelijkheid om naast de inhoud van de eigenlijke afbeelding "
"ook andere informatie te bewaren. Dit kan leesbare data zijn zoals "
"aanmaakdatum, de naam van de auteur, een beschrijving van de afbeelding, "
"maar ook door de computer leesbare data, zoals een icc-profiel wat de "
"computer vertelt over de kleurkwaliteit en kleurcorrectie in het bestand."

#: ../../general_concepts/file_formats.rst:43
msgid "Openness"
msgstr "Openheid"

#: ../../general_concepts/file_formats.rst:45
msgid ""
"This is a bit of an odd quality, but it's about how easy it to open or "
"recover the file, and how widely it's supported."
msgstr ""
"Dit is een beetje een rare kwaliteit, maar het gaat over hoe makkelijk het "
"bestand te openen of te herstellen is, en door hoeveel programma's het wordt "
"gebruikt."

#: ../../general_concepts/file_formats.rst:47
msgid ""
"Most internal file formats, like PSD are completely closed, and it's really "
"difficult for human outsiders to recover the data inside without opening "
"Photoshop. Other examples are camera raw files which have different "
"properties per camera manufacturer."
msgstr ""
"De meeste interne bestandsformaten, zoals PSD zijn compleet gesloten, en is "
"het heel moeilijk voor een gebruiker zonder Photoshop om de data te "
"herstellen. Andere voorbeelden zijn camera raw bestanden die per camera-"
"fabrikant andere eigenschappen hebben."

#: ../../general_concepts/file_formats.rst:49
msgid ""
"SVG, as a vector file format, is on the other end of the spectrum, and can "
"be opened with any text-editor and edited."
msgstr ""
"SVG, als een vector bestandsformaat, is aan de andere kant van het spectrum, "
"en kunt u met elke tekstverwerker openen en bewerken."

#: ../../general_concepts/file_formats.rst:51
msgid ""
"Most formats are in-between, and thus there's also a matter of how widely "
"supported the format is. JPG and PNG cannot be read or edited by human eyes, "
"but the vast majority of programs can open them, meaning the owner has easy "
"access to them."
msgstr ""
"De meeste bestanden zitten daar tussen in, en is het dus door hoeveel "
"programma's het formaat gebruikt wordt. JPG en PNG zijn niet door mensen "
"leesbaar, maar wel door een grote hoeveelheid programma's te openen, wat "
"inhoud dat ze voor de eigenaar goed toegankelijk zijn."

#: ../../general_concepts/file_formats.rst:53
msgid "Contents:"
msgstr "Inhoud:"
