# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 03:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<rst_epilog>:36
msgid ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: toolfreehandpath"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:1
msgid "Krita's freehand path tool reference."
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Vector"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Freehand"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:15
msgid "Freehand Path Tool"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:17
msgid "|toolfreehandpath|"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:19
msgid ""
"With the Freehand Path Tool you can draw a path (much like the :ref:"
"`shape_brush_engine`) the shape will then be filled with the selected color "
"or pattern and outlined with a brush if so chosen. While drawing a preview "
"line is shown that can be modified in pattern, width and color."
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:21
msgid ""
"This tool can be particularly good for laying in large swaths of color "
"quickly."
msgstr ""
